;;; counter.cl
;;; Shane Hale (smh1931@cs.rit.edu)
;;;  
;;; Description:
;;;  Returns a list containing the size of the original list
;;;  and all its contents

;;;
;;; counter
;;; Description:
;;;  Returns the number of items in the list
;;; Arguments:
;;;  lst - the list to count the elements
;;; Returns:
;;;  list containing the size of the original list
;;;
(defun counter (lst)
    (cond
        ;base cases
        ((not (listp lst)) NIL)
        ((null lst) '(0))

        ;call the helper function to evaluate the rest of the items in the list
        (T (append (cons (length lst) '()) (helper lst)))
    )
)

;;;
;;; helper
;;; Description:
;;;  Loops through each item in the list, and returns the length of each item
;;; Arguments:
;;;  lst - the list to count
;;; Returns:
;;;  the number of items in the list, including the number of items in any
;;;  sublists that may be in the list
;;;
(defun helper (lst)
    (cond
        ;base cases
        ((not (listp lst)) NIL)
        ((null lst) NIL)

        ;call helper for each sublist, append the result to the
        ;list that will be returned
        ((listp (car lst)) 
         (append (cons (append (cons (length (car lst)) '()) 
         (helper (car lst))) (helper (cdr lst))) '())
        )

        ;call the helper function as a last resort
        (T (helper(cdr lst)))
    )
)
